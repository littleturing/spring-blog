package cn.sunzhichao.service.impl;

import cn.sunzhichao.mapper.BlogAndTagsMapper;
import cn.sunzhichao.mapper.BlogMapper;
import cn.sunzhichao.mapper.CommentMapper;
import cn.sunzhichao.pojo.Blog;
import cn.sunzhichao.pojo.BlogAndTag;
import cn.sunzhichao.pojo.Comment;
import cn.sunzhichao.service.BlogService;
import cn.sunzhichao.util.TagsUtils;
import cn.sunzhichao.vo.BlogQuery;
import cn.sunzhichao.vo.BlogVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogMapper blogMapper;
    @Autowired
    private BlogAndTagsMapper blogAndTagsMapper;
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public Blog getBlog(Long id) {
        return blogMapper.selectById(id);
    }

    @Override
    public IPage<BlogVO> listBlog(Page<BlogVO> page) {
        return blogMapper.listBlog(page);
    }

    @Override
    public IPage<BlogVO> listBlogSearch(Page<BlogVO> page, BlogQuery blogQuery) {
        return blogMapper.listBlogSearch(page,blogQuery);
    }

    @Override
    public int saveBlog(Blog blog) {
        blog.setCreateTime(LocalDateTime.now());
        blog.setUpdateTime(LocalDateTime.now());
        blog.setViews(0);
        int res = blogMapper.insert(blog);

        //为中间表插入数据
        List<Long> tagsId = TagsUtils.convertToList(blog.getTagIds());
        for (Long tagId : tagsId) {
            BlogAndTag bat = new BlogAndTag();
            bat.setBlogId(blog.getId());
            bat.setTagId(tagId);
            blogAndTagsMapper.insert(bat);
        }
        return res;
    }

    @Override
    public int updateBlog(Blog blog) {
        blog.setUpdateTime(LocalDateTime.now());
        //修改中间表，想法是先把该blog对应的全部删除，然后再重新插入
        blogAndTagsMapper.delete(new QueryWrapper<BlogAndTag>().eq("blog_id",blog.getId()));

        List<Long> tagsId = TagsUtils.convertToList(blog.getTagIds());
        for (Long tagId : tagsId) {
            BlogAndTag bat = new BlogAndTag();
            bat.setBlogId(blog.getId());
            bat.setTagId(tagId);
            blogAndTagsMapper.insert(bat);
        }

        return blogMapper.updateById(blog);
    }

    @Override
    public int deleteBlog(Long id) {
        //删除该博客时，将所有中间表关于该博客的信息都删除
        blogAndTagsMapper.delete(new QueryWrapper<BlogAndTag>().eq("blog_id",id));
        //将记录也删除
        commentMapper.delete(new QueryWrapper<Comment>().eq("blog_id",id));
        return blogMapper.deleteById(id);
    }
}

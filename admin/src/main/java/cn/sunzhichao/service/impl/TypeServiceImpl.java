package cn.sunzhichao.service.impl;

import cn.sunzhichao.mapper.TypeMapper;
import cn.sunzhichao.pojo.Type;
import cn.sunzhichao.service.TypeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeMapper typeMapper;

    @Transactional
    @Override
    public int saveType(Type type) {
        return typeMapper.insert(type);
    }

    @Transactional
    @Override
    public Type getType(Long id) {
        return typeMapper.selectById(id);
    }

    @Transactional
    @Override
    public Type getTypeByName(String name) {
        return typeMapper.selectOne(new QueryWrapper<Type>().eq("name", name));
    }

    @Transactional
    @Override
    public IPage<Type> listType(Page<Type> page) {
        return typeMapper.selectPage(page,null);
    }

    @Transactional
    @Override
    public List<Type> listType() {
        return typeMapper.selectList(null);
    }

    @Transactional
    @Override
    public int updateType(Type type) {
        return typeMapper.updateById(type);
    }

    @Transactional
    @Override
    public int deleteType(Long id) {
        return typeMapper.deleteById(id);
    }
}

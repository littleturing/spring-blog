package cn.sunzhichao.service.impl;

import cn.sunzhichao.mapper.TagMapper;
import cn.sunzhichao.pojo.Tag;
import cn.sunzhichao.service.TagService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagMapper tagMapper;

    @Transactional
    @Override
    public int saveTag(Tag tag) {
        return tagMapper.insert(tag);
    }

    @Transactional
    @Override
    public Tag getTag(Long id) {
        return tagMapper.selectById(id);
    }

    @Transactional
    @Override
    public Tag getTagByName(String name) {
        return tagMapper.selectOne(new QueryWrapper<Tag>().eq("name", name));
    }

    @Transactional
    @Override
    public IPage<Tag> listTag(Page<Tag> page) {
        return tagMapper.selectPage(page, null);
    }

    @Transactional
    @Override
    public List<Tag> listTag() {
        return tagMapper.selectList(null);
    }

    @Transactional
    @Override
    public int updateTag(Tag tag) {
        return tagMapper.updateById(tag);
    }

    @Transactional
    @Override
    public int deleteTag(Long id) {
        return 0;
    }
}

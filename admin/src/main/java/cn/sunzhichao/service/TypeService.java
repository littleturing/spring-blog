package cn.sunzhichao.service;

import cn.sunzhichao.pojo.Type;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

import java.util.List;

public interface TypeService {

    //保存一个分类
    int saveType(Type type);

    //获得一个分类
    Type getType(Long id);

    //通过名字得到Type
    Type getTypeByName(String name);

    //分页得到所有分类
    IPage<Type> listType(Page<Type> page);

    //获得所有的分类
    List<Type> listType();

    //修改分类
    int updateType(Type type);

    //删除分类
    int deleteType(Long id);
}

package cn.sunzhichao.service;


import cn.sunzhichao.pojo.User;

public interface UserService {

    //验证登录信息
    User checkUser(String username, String password);

}

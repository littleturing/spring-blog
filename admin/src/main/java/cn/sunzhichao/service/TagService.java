package cn.sunzhichao.service;

import cn.sunzhichao.pojo.Tag;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

public interface TagService {

    //保存一个标签
    int saveTag(Tag tag);

    //获得一个标签
    Tag getTag(Long id);

    //通过名字得到Tag
    Tag getTagByName(String name);

    //分页得到所有标签
    IPage<Tag> listTag(Page<Tag> page);

    //得到所有标签
    List<Tag> listTag();

    //修改标签
    int updateTag(Tag tag);

    //删除标签
    int deleteTag(Long id);
}

package cn.sunzhichao.controller;

import cn.sunzhichao.common.Message;
import cn.sunzhichao.pojo.Blog;
import cn.sunzhichao.pojo.User;
import cn.sunzhichao.service.BlogService;
import cn.sunzhichao.service.TagService;
import cn.sunzhichao.service.TypeService;
import cn.sunzhichao.vo.BlogQuery;
import cn.sunzhichao.vo.BlogVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
public class BlogController {

    @Value("${project.pageSize}")
    private String pageSize;
    @Autowired
    private BlogService blogService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private TagService tagService;

    //跳转到博客列表页面
    @GetMapping("/blogs")
    public String blogs(@RequestParam(defaultValue = "1", name = "current") Integer current,
                        Model model) {
        IPage<BlogVO> page = blogService.listBlog(new Page<>(current, Long.parseLong(pageSize)));
        model.addAttribute("page", page);
        model.addAttribute("types", typeService.listType());
        return "blogs";
    }

    /**
     * 根据查询条件刷新表格
     * @param current 由于要分页查询，这个是当前的页数
     * @param model 用来前后端交互传递数据
     * @param blogQuery 把前端发送过来的参数封装成一个blogQuery对象
     * @return 路径
     */
    @PostMapping("/blogs/search")
    public String search(@RequestParam(defaultValue = "1", name = "current") Integer current,
                         Model model,
                         BlogQuery blogQuery) {
        IPage<BlogVO> page = blogService.listBlogSearch(new Page<>(current, Long.parseLong(pageSize)),blogQuery);
        model.addAttribute("page", page);
        //这样返回只会刷新blogList，不会返回全部的页面数据
        return "blogs :: blogList";
    }

    //查询分类和标签
    private void queryTypeAndTag(Model model) {
        model.addAttribute("types", typeService.listType());
        model.addAttribute("tags",tagService.listTag());
    }

    //跳转到博客发布页面
    @GetMapping("/blogs/input")
    public String input(Model model) {
        queryTypeAndTag(model);
        model.addAttribute("blog", new Blog());
        return "blogs-input";
    }

    //携带Blog数据跳转到编辑页面
    @GetMapping("/blogs/{id}/edit")
    public String edit(@PathVariable Long id, Model model) {
        queryTypeAndTag(model);
        model.addAttribute("blog", blogService.getBlog(id));
        return "blogs-input";
    }

    //保存Blog
    //如果没有id则为新增，有id则为修改
    @PostMapping("/blogs/save")
    public String save(Blog blog, HttpSession session, RedirectAttributes attributes) {
        User user = (User) session.getAttribute("user");
        blog.setUserId(user.getId());

        int i;
        //修改
        if(blog.getId() != null){
            i = blogService.updateBlog(blog);
            if(i > 0){
                attributes.addFlashAttribute("message", Message.SUCCESS_EDIT);
            } else {
                attributes.addFlashAttribute("message",Message.FAILED_EDIT);
            }
        } else {
            i = blogService.saveBlog(blog);
            if(i > 0){
                attributes.addFlashAttribute("message",Message.SUCCESS_ADD);
            } else {
                attributes.addFlashAttribute("message",Message.FAILED_ADD);
            }
        }

        return "redirect:/admin/blogs";
    }

    @GetMapping("/blogs/{id}/delete")
    public String delete(@PathVariable Long id, RedirectAttributes attributes) {
        int i = blogService.deleteBlog(id);
        if(i < 1){
            attributes.addFlashAttribute("message",Message.FAILED_DEL);
        } else {
            attributes.addFlashAttribute("message",Message.SUCCESS_DEL);
        }
        return "redirect:/admin/blogs";    }
}

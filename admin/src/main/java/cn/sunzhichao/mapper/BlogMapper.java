package cn.sunzhichao.mapper;

import cn.sunzhichao.pojo.Blog;
import cn.sunzhichao.vo.BlogQuery;
import cn.sunzhichao.vo.BlogVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BlogMapper extends BaseMapper<Blog> {

    //分页查询所有
    IPage<BlogVO> listBlog(Page<BlogVO> page);

    //根据BlogVO进行条件查询
    IPage<BlogVO> listBlogSearch(Page<BlogVO> page, BlogQuery blogQuery);
}

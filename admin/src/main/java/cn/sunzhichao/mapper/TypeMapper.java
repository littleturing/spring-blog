package cn.sunzhichao.mapper;

import cn.sunzhichao.pojo.Type;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeMapper extends BaseMapper<Type> {
}

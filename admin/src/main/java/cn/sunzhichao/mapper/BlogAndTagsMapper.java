package cn.sunzhichao.mapper;

import cn.sunzhichao.pojo.BlogAndTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BlogAndTagsMapper extends BaseMapper<BlogAndTag> {

}

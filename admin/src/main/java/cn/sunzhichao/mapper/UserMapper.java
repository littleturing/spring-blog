package cn.sunzhichao.mapper;

import cn.sunzhichao.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @Author manster
 * @Date 2021/4/22
 **/
@Repository
public interface UserMapper extends BaseMapper<User> {

}

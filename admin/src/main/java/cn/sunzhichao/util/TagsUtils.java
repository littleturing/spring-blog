package cn.sunzhichao.util;

import cn.sunzhichao.pojo.Tag;

import java.util.ArrayList;
import java.util.List;

public class TagsUtils {

    //将 1,2,3 转换为 [1,2,3]
    public static List<Long> convertToList(String ids){
        List<Long> list = new ArrayList<>();
        if(!"".equals(ids) && ids != null){
            String[] idArr = ids.split(",");
            for (String s : idArr) {
                list.add(new Long(s));
            }
        }
        return list;
    }
}

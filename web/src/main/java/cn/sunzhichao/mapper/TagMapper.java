package cn.sunzhichao.mapper;

import cn.sunzhichao.pojo.Tag;
import cn.sunzhichao.vo.TagVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TagMapper extends BaseMapper<Tag> {

    //查询所有数量的type
    List<TagVO> listTagVO();
}

package cn.sunzhichao.mapper;

import cn.sunzhichao.pojo.Blog;
import cn.sunzhichao.vo.BlogVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BlogMapper extends BaseMapper<Blog> {

    //分页查询所有简单信息
    IPage<BlogVO> listBlog(Page<BlogVO> page);

    //分页查询搜索的blog,模糊查询 title 和 description
    IPage<BlogVO> listSearchBlog(Page<BlogVO> page, String query);

    //查询关于blog的一切
    BlogVO getBlogVO(Long id);

    //增加浏览量
    int addViews(Long id);

    //根据分类id查询blog
    IPage<BlogVO> listBlogByType(Page<BlogVO> page, Long id);

    //获取所有年份
    List<String> getGroupYears();

    //根据年份获得博客
    List<Blog> getBlogByYear(String year);

    //获取最新博客
    List<Blog> selectNewBlogs();
}

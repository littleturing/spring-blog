package cn.sunzhichao.mapper;

import cn.sunzhichao.pojo.Type;
import cn.sunzhichao.vo.TypeVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TypeMapper extends BaseMapper<Type> {

    //查询所有的type
    List<TypeVO> listTypeVO();
}

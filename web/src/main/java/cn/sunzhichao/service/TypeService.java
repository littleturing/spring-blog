package cn.sunzhichao.service;

import cn.sunzhichao.vo.TypeVO;

import java.util.List;

public interface TypeService {

    //列出所有分类及其所拥有的博客
    List<TypeVO> listTypeVO();

}

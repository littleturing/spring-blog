package cn.sunzhichao.service;

import cn.sunzhichao.pojo.Comment;

import java.util.List;

public interface CommentService {

    //获取博客评论列表
    List<Comment> listCommentByBlogId(Long blogId);

    //保存一条评论
    int saveComment(Comment comment, String avatar);
}

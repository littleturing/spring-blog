package cn.sunzhichao.service.impl;

import cn.sunzhichao.mapper.TypeMapper;
import cn.sunzhichao.service.TypeService;
import cn.sunzhichao.vo.TypeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeMapper typeMapper;

    @Override
    public List<TypeVO> listTypeVO() {
        return typeMapper.listTypeVO();
    }
}

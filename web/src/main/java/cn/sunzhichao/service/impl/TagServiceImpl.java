package cn.sunzhichao.service.impl;

import cn.sunzhichao.mapper.TagMapper;
import cn.sunzhichao.service.TagService;
import cn.sunzhichao.vo.TagVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagMapper tagMapper;

    @Override
    public List<TagVO> listTagVO() {
        return tagMapper.listTagVO();
    }
}

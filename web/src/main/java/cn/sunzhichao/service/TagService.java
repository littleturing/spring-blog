package cn.sunzhichao.service;

import cn.sunzhichao.vo.TagVO;

import java.util.List;

public interface TagService {

    //列出所有标签及其所拥有的博客
    List<TagVO> listTagVO();
}

package cn.sunzhichao.vo;

import cn.sunzhichao.pojo.Blog;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class TypeVO implements Serializable {

    private Long id;
    private String name;

    private List<Blog> blogs = new ArrayList<>();

    private static final long serialVersionUID = 1L;
}

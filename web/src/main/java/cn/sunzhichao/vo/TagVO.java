package cn.sunzhichao.vo;

import cn.sunzhichao.pojo.Blog;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class TagVO implements Serializable {

    private Long id;
    private String name;
    private List<Blog> blogs = new ArrayList<>();//该标签下的所有blog

    private static final long serialVersionUID = 1L;
}
